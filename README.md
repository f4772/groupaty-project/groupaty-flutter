# groupaty_flutter

The Flutter part of Groupaty project.

## Getting Started

To launch the app, simply cd into the project folder then run "flutter run"

## Note

Make sure to run the server in Groupaty-API folder first
