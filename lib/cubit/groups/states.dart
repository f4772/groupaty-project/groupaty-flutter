abstract class GroupsStates {}

class GroupsInitialState extends GroupsStates {}

class FetchUserGroupsLoadingState extends GroupsStates {}

class FetchUserGroupsSuccessState extends GroupsStates {}

class FetchUserGroupsErrorState extends GroupsStates {
  String error;
  FetchUserGroupsErrorState(this.error);
}


class FetchGroupPostsLoadingState extends GroupsStates {}

class FetchGroupPostsSuccessState extends GroupsStates {}

class FetchGroupPostsErrorState extends GroupsStates {
  String error;
  FetchGroupPostsErrorState(this.error);
}


class AddGroupPostsLoadingState extends GroupsStates {}

class AddGroupPostsSuccessState extends GroupsStates {}

class AddGroupPostsErrorState extends GroupsStates {
  String error;
  AddGroupPostsErrorState(this.error);
}

class AddGroupLoadingState extends GroupsStates{}

class AddGroupSuccessState extends GroupsStates {}

class AddGroupErrorState extends GroupsStates {
  String error;
  AddGroupErrorState(this.error);
}


class RemoveUserFromGroupSucessState extends GroupsStates{}