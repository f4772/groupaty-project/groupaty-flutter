import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/cubit/groups/states.dart';
import 'package:groupaty_flutter/models/Group.dart';
import 'package:groupaty_flutter/models/Post.dart';
import 'package:groupaty_flutter/models/User.dart';
import 'package:groupaty_flutter/models/UserGroup.dart';
import 'package:groupaty_flutter/network/api_manager.dart';
import 'package:groupaty_flutter/network/cache_manager.dart';
import 'package:groupaty_flutter/network/endpoints.dart';

class GroupsCubit extends Cubit<GroupsStates> {
  GroupsCubit() : super(GroupsInitialState());
  static GroupsCubit get(context) => BlocProvider.of(context);

  final DioHelper _dioHelper = DioHelper();
  final CacheManager _cacheManager = CacheManager();

  List<GroupModel> userGroups = [];

  Future<void> getUserGroups() async {
    emit(FetchUserGroupsLoadingState());
    userGroups = [];
    String id = _cacheManager.getData("ID");
    _dioHelper
        .get(path: Endpoints.user + "/$id", query: {}).then((value) async {
      UserModel user = UserModel.fromJson(value);
      await fetchGroups(user);
      await getAllGroups();
      emit(FetchUserGroupsSuccessState());
    });
  }

  Future<void> fetchGroups(UserModel user) async {
    for (UserGroupModel userGroup in user.groups ?? []) {
      await _dioHelper.get(
          path: Endpoints.groups + "/${userGroup.group_id}",
          query: {}).then((value) {
        GroupModel group = GroupModel.fromJson(value);
        userGroups.add(group);
      });
    }
  }

  List<GroupModel> allGroups = [];
  Future<void> getAllGroups() async {
    _dioHelper.get(path: Endpoints.groups, query: {}).then((value) {
      allGroups =
          List<GroupModel>.from(value.map((x) => GroupModel.fromJson(x)));
    });
  }

  List<PostModel> groupPosts = [];

  Future<void> getGroupPosts(String groupID) async {
    emit(FetchGroupPostsLoadingState());
    _dioHelper.get(path: Endpoints.groups + "/$groupID/posts", query: {}).then(
        (value) async {
      await parsePosts(value);
      emit(FetchGroupPostsSuccessState());
    }).catchError((onError) {
      emit(FetchGroupPostsErrorState(onError.toString()));
    });
  }

  Future<void> parsePosts(dynamic json) async {
    groupPosts = List<PostModel>.from(json.map((x) => PostModel.fromJson(x)));
  }

  Future<void> addUserToGroup(String groupID) async {
    String id = _cacheManager.getData("ID");
    _dioHelper.patch(
        path: Endpoints.groups + "/$id/join",
        data: {"group_id": groupID}).then((value) {
      getUserGroups();
    });
  }

  Future<void> removeUserFromGroup(String groupID) async {
    String id = _cacheManager.getData("ID");
    _dioHelper.patch(
        path: Endpoints.groups + "/$id/leave",
        data: {"group_id": groupID}).then((value) {
      getUserGroups().then((value) {
        emit(RemoveUserFromGroupSucessState());
      });
    });
  }

  void addPost(String post, String groupID) async {
    emit(AddGroupPostsLoadingState());
    String id = _cacheManager.getData("ID");
    _dioHelper.post(path: Endpoints.posts, data: {
      "post": post,
      "user_id": id,
      "group_id": groupID,
    }).then((value) {
      getGroupPosts(groupID);
      emit(AddGroupPostsSuccessState());
    }).catchError((onError) {
      print(onError.toString());
      emit(AddGroupPostsErrorState(onError.toString()));
    });
  }

  void createGroup(String groupName) async {
    emit(AddGroupLoadingState());
    _dioHelper.post(path: Endpoints.groups, data: {"name": groupName}).then(
        (value) async {
      await getUserGroups();
      emit(AddGroupSuccessState());
    }).catchError((onError) {
      emit(AddGroupErrorState(onError.toString()));
    });
  }
}
