abstract class HomeStates{}

class HomeInitialState extends HomeStates{}

class FetchNewsFeedPostsLoadingState extends HomeStates{}
class FetchNewsFeedPostsSuccessState extends HomeStates{}
class FetchNewsFeedPostsErrorState extends HomeStates{}