import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/cubit/home/states.dart';
import 'package:groupaty_flutter/models/Group.dart';
import 'package:groupaty_flutter/models/Post.dart';
import 'package:groupaty_flutter/models/User.dart';
import 'package:groupaty_flutter/models/UserGroup.dart';
import 'package:groupaty_flutter/network/api_manager.dart';
import 'package:groupaty_flutter/network/cache_manager.dart';
import 'package:groupaty_flutter/network/endpoints.dart';

class HomeCubit extends Cubit<HomeStates> {
  HomeCubit() : super(HomeInitialState());
  static HomeCubit get(context) => BlocProvider.of(context);

  final DioHelper _dioHelper = DioHelper();
  final CacheManager _cacheManager = CacheManager();

  List<PostModel> newsFeedPosts = [];

  Future<void> getNewsfeedPosts() async {
    emit(FetchNewsFeedPostsLoadingState());
    String id = _cacheManager.getData("ID");
    _dioHelper.get(path: Endpoints.user + "/$id/newsfeed", query: {}).then(
        (value) async {
      await parsePosts(value);
      emit(FetchNewsFeedPostsSuccessState());
    }).catchError((onError) {
      emit(FetchNewsFeedPostsErrorState());
    });
  }

  Future<void> parsePosts(dynamic json) async {
    newsFeedPosts =
        List<PostModel>.from(json.map((x) => PostModel.fromJson(x)));
  }
}
