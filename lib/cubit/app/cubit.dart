import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/groups/groups_screen.dart';
import 'package:groupaty_flutter/UI/home/home_screen.dart';
import 'package:groupaty_flutter/UI/profile/profile_screen.dart';
import 'package:groupaty_flutter/cubit/app/states.dart';
import 'package:groupaty_flutter/network/cache_manager.dart';

class AppCubit extends Cubit<AppStates> {
  AppCubit() : super(AppInitialState());
  CacheManager _cacheManager = CacheManager();
  static AppCubit get(context) => BlocProvider.of(context);

  List<Widget> screens = [
    HomeScreen(),
    GroupsScreen(),
    ProfileScreen(),
  ];

  int currentIndex = 0;

  void switchIndex(int index) {
    currentIndex = index;
    emit(NavbarChangeState());
  }

  void checkForCacheduser() async {
    await Future.delayed(const Duration(seconds: 2));
    String? id = _cacheManager.getData("ID");
    emit(CachedUserStateIs(userID: id));
  }
}
