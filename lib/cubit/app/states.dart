abstract class AppStates {}

class AppInitialState extends AppStates {}

class NavbarChangeState extends AppStates {}

class CachedUserStateIs extends AppStates {
  String? userID;
  CachedUserStateIs({this.userID});
}
