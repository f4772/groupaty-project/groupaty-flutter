abstract class ProfileStates {}

class ProfileInitialState extends ProfileStates {}

class FetchUserProfileLoadingState extends ProfileStates {}

class FetchUserProfileSuccessState extends ProfileStates {}

class FetchUserProfileErrorState extends ProfileStates {
  String error;
  FetchUserProfileErrorState(this.error);
}

class LogoutSuccessState extends ProfileStates{}