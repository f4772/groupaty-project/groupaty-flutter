import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/cubit/profile/states.dart';
import 'package:groupaty_flutter/models/Post.dart';
import 'package:groupaty_flutter/models/User.dart';
import 'package:groupaty_flutter/network/api_manager.dart';
import 'package:groupaty_flutter/network/cache_manager.dart';
import 'package:groupaty_flutter/network/endpoints.dart';

class ProfileCubit extends Cubit<ProfileStates> {
  ProfileCubit() : super(ProfileInitialState());
  static ProfileCubit get(context) => BlocProvider.of(context);

  final DioHelper _dioHelper = DioHelper();
  final CacheManager _cacheManager = CacheManager();

  UserModel? user;

  Future<void> getUserData() async {
    emit(FetchUserProfileLoadingState());
    String id = _cacheManager.getData("ID");
    _dioHelper
        .get(path: Endpoints.user + "/$id", query: {}).then((value) async {
      user = UserModel.fromJson(value);
      await getUserPosts(id);
      
    }).catchError((onError) {
      emit(FetchUserProfileErrorState(onError.toString()));
    });
  }

  void logout() {
    _cacheManager.clearData(key: "ID").then((value) {
      emit(LogoutSuccessState());
    });
  }

  List<PostModel> userPosts = [];

  Future<void> getUserPosts(String id) async {
    _dioHelper
        .get(path: Endpoints.user + "/$id/posts", query: {}).then((value) {
      userPosts = List<PostModel>.from(value.map((x) => PostModel.fromJson(x)));
      emit(FetchUserProfileSuccessState());
    });
  }
}
