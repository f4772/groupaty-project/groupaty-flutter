abstract class AuthStates{}

class AuthInitialState extends AuthStates{}

class LoginLoadingState extends AuthStates{}
class LoginSuccessState extends AuthStates{}
class LoginErrorState extends AuthStates{
  String error;
  LoginErrorState(this.error);
}

class RegisterLoadingState extends AuthStates{}
class RegisterSuccessState extends AuthStates{}
class RegisterErrorState extends AuthStates{
  String error;
  RegisterErrorState(this.error);
}