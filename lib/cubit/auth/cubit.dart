import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/cubit/auth/states.dart';
import 'package:groupaty_flutter/models/User.dart';
import 'package:groupaty_flutter/network/api_manager.dart';
import 'package:groupaty_flutter/network/cache_manager.dart';
import 'package:groupaty_flutter/network/endpoints.dart';

class AuthCubit extends Cubit<AuthStates> {
  AuthCubit() : super(AuthInitialState());
  static AuthCubit get(context) => BlocProvider.of(context);
  final DioHelper _dioHelper = DioHelper();
  final CacheManager _cacheManager = CacheManager();

  Future<void> login({required String email, required String password}) async {
    emit(LoginLoadingState());
    _dioHelper.post(
        path: Endpoints.login,
        data: {"email": email, "password": password}).then((value) {
      print(value);
      UserModel user = UserModel.fromJson(value);
      _cacheManager.saveData(key:"ID",value:user.id);
      emit(LoginSuccessState());
    }).catchError((onError) {
      print(onError.toString());
      emit(LoginErrorState(onError.toString()));
    });
  }

  Future<void> register(
      {required String name,
      required String email,
      required String password,
      String? bio}) async {
    emit(RegisterLoadingState());
    _dioHelper.post(path: Endpoints.register, data: {
      "name": name,
      "email": email,
      "password": password,
      "bio": bio
    }).then((value) {
      emit(RegisterSuccessState());
      print(value);
    }).catchError((onError) {
      if (onError is DioError) {
        print(onError.response);
        emit(RegisterErrorState(
            "Registeration error: " + onError.response.toString()));
      }
    });
  }
}
