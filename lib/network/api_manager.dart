import 'package:dio/dio.dart';

class DioHelper {
  static late Dio dio;

  static init() {
    dio = Dio(
      BaseOptions(
          baseUrl: "http://localhost:5000/",
          receiveDataWhenStatusError: true,
          headers: {"Content-Type": "application/json"}),
    );
  }

  void addHeaders(Map<String, dynamic> headers) {
    dio.options.headers.addAll(headers);
  }

  Future<dynamic> get({
    required String path,
    required Map<String, dynamic> query,
  }) async {
    Response response = await dio.get(path, queryParameters: query);
    return response.data;
  }

  Future<dynamic> post({
    required String path,
    required Map<String, dynamic> data,
    Map<String, dynamic>? query,
  }) async {
    Response response = await dio.post(path, queryParameters: query, data: data);
    return response.data;
  }

  Future<dynamic> patch({
    required String path,
    required Map<String, dynamic> data,
    Map<String, dynamic>? query,
  }) async {
    Response response = await dio.patch(path, queryParameters: query, data: data);
    return response.data;
  }

  Future<dynamic> delete({
    required String path,
    Map<String, dynamic>? query,
  }) async {
    Response response = await dio.delete(path, queryParameters: query);
    return response.data;
  }
}
