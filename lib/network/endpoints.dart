class Endpoints {
  static const String login = "users/login";
  static const String register = "users/register";
  static const String user = "users";
  static const String groups = "groups";
  static const String posts = "posts";
}
