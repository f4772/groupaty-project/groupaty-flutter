import 'package:buildcondition/buildcondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/home/single_newsfeed_post_widget.dart';
import 'package:groupaty_flutter/cubit/home/cubit.dart';
import 'package:groupaty_flutter/cubit/home/states.dart';
import 'package:groupaty_flutter/models/Post.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      HomeCubit.get(context).getNewsfeedPosts();
      return BlocConsumer<HomeCubit, HomeStates>(
        listener: (context, state) {},
        builder: (context, state) {
          List<PostModel> newsFeedPosts = HomeCubit.get(context).newsFeedPosts;
          return BuildCondition(
            condition: state is! FetchNewsFeedPostsLoadingState,
            fallback: (context) => Center(child: CircularProgressIndicator()),
            builder: (context) => SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:12.0),
                      child: Text(
                        "News Feed",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) =>
                          SingleNewsfeedPostWidget(post: newsFeedPosts[index]),
                      itemCount: newsFeedPosts.length,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }
}
