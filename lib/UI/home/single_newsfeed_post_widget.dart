import 'package:flutter/material.dart';
import 'package:groupaty_flutter/models/Post.dart';

class SingleNewsfeedPostWidget extends StatelessWidget {
  const SingleNewsfeedPostWidget({Key? key, required this.post})
      : super(key: key);
  final PostModel post;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    post.user_name ?? '',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    post.group_name ?? '',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  post.post ?? '',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Text(post.date ?? ''),
            ],
          ),
        ),
      ),
    );
  }
}
