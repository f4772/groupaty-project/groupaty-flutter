import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/shared_widgets/toast.dart';
import 'package:groupaty_flutter/cubit/groups/cubit.dart';
import 'package:groupaty_flutter/cubit/groups/states.dart';
import 'package:groupaty_flutter/resources/strings.dart';

class CreatePostScreen extends StatelessWidget {
  CreatePostScreen({Key? key, required this.groupID}) : super(key: key);
  final String groupID;
  final TextEditingController postController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GroupsCubit, GroupsStates>(listener: (context, state) {
      if (state is AddGroupPostsSuccessState) {
        showToast(
            message: "Post Added Successfully", state: ToastStates.SUCCESS);
        Navigator.pop(context);
      }
    }, builder: (context, state) {
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.done,
          ),
          onPressed: () {
            GroupsCubit.get(context).addPost(postController.text, groupID);
          },
        ),
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
          child: TextFormField(
            keyboardType: TextInputType.text,
            controller: postController,
            maxLines: 7,
            decoration: InputDecoration(
                hintText: AppStrings.whatYouHaveInMind,
                border: InputBorder.none),
          ),
        ),
      );
    });
  }
}
