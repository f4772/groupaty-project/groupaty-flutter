import 'package:flutter/material.dart';
import 'package:groupaty_flutter/UI/groups/group_screen.dart';
import 'package:groupaty_flutter/cubit/groups/cubit.dart';
import 'package:groupaty_flutter/models/Group.dart';
import 'package:groupaty_flutter/shared_functions.dart';

class GroupItem extends StatelessWidget {
  const GroupItem({Key? key, required this.group, required this.isUserJoined})
      : super(key: key);
  final GroupModel group;
  final bool isUserJoined;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            group.name ?? '',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w700,
            ),
          ),
          InkWell(
            onTap: () {
              if (isUserJoined) {
                navigate(
                    GroupScreen(
                      group: group,
                    ),
                    context);
              } else {
                GroupsCubit.get(context).addUserToGroup(group.id ?? '');
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 18),
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                isUserJoined ? "View" : "Join",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
