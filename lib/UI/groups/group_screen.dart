import 'package:buildcondition/buildcondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/groups/create_post_screen.dart';
import 'package:groupaty_flutter/cubit/groups/cubit.dart';
import 'package:groupaty_flutter/cubit/groups/states.dart';
import 'package:groupaty_flutter/models/Group.dart';
import 'package:groupaty_flutter/models/Post.dart';
import 'package:groupaty_flutter/resources/strings.dart';
import 'package:groupaty_flutter/shared_functions.dart';

class GroupScreen extends StatelessWidget {
  const GroupScreen({Key? key, required this.group}) : super(key: key);
  final GroupModel group;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      GroupsCubit.get(context).getGroupPosts(group.id ?? '');
      return BlocConsumer<GroupsCubit, GroupsStates>(
        listener: (context, state) {
          if (state is RemoveUserFromGroupSucessState) {
            Navigator.pop(context);
            // Navigator.pop(context);
          }
        },
        builder: (context, state) {
          List<PostModel> posts = GroupsCubit.get(context).groupPosts;
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              elevation: 0,
              title: Text(
                group.name ?? '',
                style: TextStyle(color: Colors.black),
              ),
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              actions: [
                IconButton(
                  icon: Icon(Icons.close, color: Colors.red),
                  onPressed: () {
                    // showDialog(
                    //     context: context,
                    //     builder: (context) =>
                    //         _buildLeaveGroupDialog(context, group.id ?? ''));
                    GroupsCubit.get(context).removeUserFromGroup(group.id??'');
                  },
                ),
              ],
            ),
            body: BuildCondition(
                condition: state is! FetchGroupPostsLoadingState,
                fallback: (context) =>
                    Center(child: CircularProgressIndicator()),
                builder: (context) => Scaffold(
                      floatingActionButton: FloatingActionButton(
                        child: Icon(Icons.add),
                        onPressed: () {
                          navigate(
                              CreatePostScreen(
                                groupID: group.id ?? '',
                              ),
                              context);
                        },
                      ),
                      body: Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: ListView.builder(
                            itemCount: posts.length,
                            itemBuilder: (context, index) =>
                                _buildSinglePost(posts[index])),
                      ),
                    )),
          );
        },
      );
    });
  }

  Widget _buildSinglePost(PostModel post) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(post.user_name ?? ''),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  post.post ?? '',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Text(post.date ?? ''),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLeaveGroupDialog(BuildContext context, String groupID) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: Center(
                    child: Text(
                      AppStrings.leaveGroupMessage,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(children: <Widget>[
              Expanded(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 50,
                    child: Center(
                      child: Text(
                        AppStrings.cancel,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    GroupsCubit.get(context).removeUserFromGroup(groupID);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    height: 50,
                    child: Center(
                      child: Text(
                        AppStrings.ok,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}
