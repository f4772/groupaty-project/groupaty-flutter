import 'package:buildcondition/buildcondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/groups/group_item.dart';
import 'package:groupaty_flutter/cubit/groups/cubit.dart';
import 'package:groupaty_flutter/cubit/groups/states.dart';
import 'package:groupaty_flutter/models/Group.dart';
import 'package:groupaty_flutter/resources/strings.dart';

class GroupsScreen extends StatelessWidget {
  const GroupsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      GroupsCubit.get(context).getUserGroups();
      GroupsCubit.get(context).getAllGroups();
      return BlocConsumer<GroupsCubit, GroupsStates>(
        listener: (context, state) {
          if (state is AddGroupSuccessState) {
            Navigator.pop(context);
          }
        },
        builder: (context, state) {
          List<GroupModel> userGroups = GroupsCubit.get(context).userGroups;
          List<GroupModel> allGroups = GroupsCubit.get(context).allGroups;
          return BuildCondition(
              condition: state is! FetchUserGroupsLoadingState &&
                  state is! AddGroupLoadingState,
              fallback: (context) => Center(child: CircularProgressIndicator()),
              builder: (context) => Scaffold(
                    floatingActionButton: FloatingActionButton(
                      child: Icon(Icons.add),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (_) => AlertDialog(
                                  title: Text('Add new group'),
                                  content: TextFormField(
                                    keyboardType: TextInputType.text,
                                    onFieldSubmitted: (val) {
                                      GroupsCubit.get(context).createGroup(val);
                                    },
                                    decoration: InputDecoration(
                                        label: Text(AppStrings.groupName),
                                        border: InputBorder.none),
                                  ),
                                ));
                      },
                    ),
                    body: Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: ListView.builder(
                        itemCount: allGroups.length,
                        itemBuilder: (context, index) => GroupItem(
                          group: allGroups[index],
                          isUserJoined: checkIfUserJoinedGroup(
                              allGroups[index], userGroups),
                        ),
                      ),
                    ),
                  ));
        },
      );
    });
  }

  bool checkIfUserJoinedGroup(GroupModel group, List<GroupModel> allgroups) {
    for (GroupModel singleGroup in allgroups) {
      if (singleGroup.id == group.id) {
        return true;
      }
    }
    return false;
  }
}
