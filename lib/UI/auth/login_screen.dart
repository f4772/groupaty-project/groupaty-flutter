import 'package:buildcondition/buildcondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/auth/register_screen.dart';
import 'package:groupaty_flutter/UI/main_screen.dart';
import 'package:groupaty_flutter/UI/shared_widgets/default_button.dart';
import 'package:groupaty_flutter/UI/shared_widgets/text_button.dart';
import 'package:groupaty_flutter/UI/shared_widgets/toast.dart';
import 'package:groupaty_flutter/cubit/auth/cubit.dart';
import 'package:groupaty_flutter/cubit/auth/states.dart';
import 'package:groupaty_flutter/resources/strings.dart';
import 'package:groupaty_flutter/shared_functions.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return BlocConsumer<AuthCubit, AuthStates>(
        listener: (context, state) {
          if (state is LoginSuccessState) {
            navigateAndClear(MainScreen(), context);
          }
          if (state is LoginErrorState) {
            showToast(message: AppStrings.loginError, state: ToastStates.ERROR);
          }
        },
        builder: (context, state) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 200.0),
                      child: Center(
                        child: Text(
                          "GROUPATY",
                          style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      AppStrings.loginMessage,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          ?.copyWith(color: Colors.grey, fontSize: 20),
                    ),
                    SizedBox(height: 50),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        validator: (val) {
                          if (val?.isEmpty ?? false) {
                            return AppStrings.emailRequired;
                          }
                        },
                        decoration: InputDecoration(
                          label: Text(AppStrings.email),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        controller: passController,
                        obscureText: true,
                        validator: (val) {
                          if (val?.isEmpty ?? false) {
                            return AppStrings.passwordTooShort;
                          }
                        },
                        decoration: InputDecoration(
                          label: Text(AppStrings.password),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    SizedBox(height: 25),
                    BuildCondition(
                      condition: state is! LoginLoadingState,
                      builder: (context) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12.0),
                        child: defaultButton(
                            function: () {
                              if (formKey.currentState?.validate() ?? false) {
                                AuthCubit.get(context).login(
                                    email: emailController.text,
                                    password: passController.text);
                              }
                            },
                            text: AppStrings.login.toUpperCase()),
                      ),
                      fallback: (context) => Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical:8),
                      child: Row(
                        children: [
                          Text(
                            AppStrings.dontHaveAccount,
                          ),
                          defaultTextButton(function: (){
                            navigate(RegisterScreen(),context);
                          }, text: AppStrings.registerNow)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }
}
