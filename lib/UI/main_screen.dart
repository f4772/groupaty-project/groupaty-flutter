import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/cubit/app/cubit.dart';
import 'package:groupaty_flutter/cubit/app/states.dart';
import 'package:groupaty_flutter/resources/strings.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
      listener: (context, state) {},
      builder: (context, state) {
        return Scaffold(
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: AppCubit.get(context).currentIndex,
            onTap: (index){
              AppCubit.get(context).switchIndex(index);
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label:AppStrings.home,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.groups),
                label:AppStrings.groups,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label:AppStrings.profile,
              ),
            ],
          ),
          body: AppCubit.get(context).screens[AppCubit.get(context).currentIndex],
        );
      },
    );
  }
}
