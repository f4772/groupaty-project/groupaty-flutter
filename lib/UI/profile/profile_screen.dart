import 'package:buildcondition/buildcondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/auth/login_screen.dart';
import 'package:groupaty_flutter/UI/profile/single_post_widget.dart';
import 'package:groupaty_flutter/cubit/app/cubit.dart';
import 'package:groupaty_flutter/cubit/profile/cubit.dart';
import 'package:groupaty_flutter/cubit/profile/states.dart';
import 'package:groupaty_flutter/models/Post.dart';
import 'package:groupaty_flutter/models/User.dart';
import 'package:groupaty_flutter/shared_functions.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      ProfileCubit.get(context).getUserData();
      return BlocConsumer<ProfileCubit, ProfileStates>(
        listener: (context, state) {
          if (state is LogoutSuccessState) {
            navigateAndClear(LoginScreen(), context);
            AppCubit.get(context).currentIndex = 0;
          }
        },
        builder: (context, state) {
          UserModel? user = ProfileCubit.get(context).user;
          List<PostModel> userPosts = ProfileCubit.get(context).userPosts;
          return BuildCondition(
              condition: ProfileCubit.get(context).user != null,
              fallback: (context) => Center(
                    child: CircularProgressIndicator(),
                  ),
              builder: (context) {
                return Scaffold(
                    appBar: AppBar(
                      backgroundColor:
                          Theme.of(context).scaffoldBackgroundColor,
                      elevation: 0,
                      leading: IconButton(
                        icon: Icon(
                          Icons.logout,
                          color: Colors.grey,
                          size: 28,
                        ),
                        onPressed: () {
                          ProfileCubit.get(context).logout();
                        },
                      ),
                    ),
                    body: BuildCondition(
                      condition: state is! FetchUserProfileLoadingState,
                      fallback: (context) => Center(
                        child: CircularProgressIndicator(),
                      ),
                      builder: (context) => SingleChildScrollView(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 16),
                              child: Center(
                                child: Text(
                                  user?.name ?? '',
                                  style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Center(
                                child: Text(
                                  user?.email ?? '',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                            Visibility(
                              visible: user?.bio != null,
                              child: Center(
                                child: Text(
                                  user?.bio ?? '',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 10.0, bottom: 12),
                              child: Divider(color: Colors.black),
                            ),
                            _buildUserPostsSection(userPosts),
                          ],
                        ),
                      ),
                    ));
              });
        },
      );
    });
  }

  Widget _buildUserPostsSection(List<PostModel> userPosts) {
    return Visibility(
      visible: userPosts.isNotEmpty,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12),
            child: Text(
              "User posts",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            itemBuilder: (context, index) =>
                SinglePostWidget(post: userPosts[index]),
            itemCount: userPosts.length,
          ),
        ],
      ),
    );
  }
}
