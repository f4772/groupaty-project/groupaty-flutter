import 'package:flutter/material.dart';
import 'package:groupaty_flutter/models/Post.dart';

class SinglePostWidget extends StatelessWidget {
  const SinglePostWidget({Key? key, required this.post}) : super(key: key);
  final PostModel post;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(post.group_name ?? ''),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  post.post ?? '',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Text(post.date ?? ''),
            ],
          ),
        ),
      ),
    );
  }
}
