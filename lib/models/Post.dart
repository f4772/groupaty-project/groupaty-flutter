class PostModel {
  String? id;
  String? post;
  String? userID;
  String? groupID;
  String? date;
  String? user_name;
  String? group_name;

  PostModel({
    this.id,
    this.post,
    this.userID,
    this.groupID,
    this.date,
    this.user_name,
    this.group_name,
  });

  factory PostModel.fromJson(Map<String, dynamic> json) => PostModel(
        id: json["_id"],
        post: json["post"],
        userID: json["userID"],
        groupID: json["groupID"],
        date: json["date"],
        user_name: json["user_name"],
        group_name: json["group_name"],
      );
}
