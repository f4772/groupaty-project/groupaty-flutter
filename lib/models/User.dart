import 'package:groupaty_flutter/models/UserGroup.dart';

class UserModel {
  String? id;
  String? name;
  String? email;
  String? bio;
  List<UserGroupModel>? groups;

  UserModel({this.id, this.name, this.email, this.bio, this.groups});
  
  factory UserModel.fromJson(Map<String,dynamic> json) => UserModel(
    id:json["_id"],
    name: json["name"],
    email: json["email"],
    bio: json["bio"],
    groups: List<UserGroupModel>.from(json["groups"].map((x) => UserGroupModel.fromJson(x))),
  );
}
