class GroupModel {
  String? id;
  String? name;

  GroupModel({this.id, this.name});

  factory GroupModel.fromJson(Map<String, dynamic> json) => GroupModel(
        id: json["_id"],
        name: json["name"],
      );
}
