class UserGroupModel {
  String? id;
  String? group_id;

  UserGroupModel({this.id, this.group_id});

  factory UserGroupModel.fromJson(Map<String,dynamic> json) =>UserGroupModel(
    id:json["_id"],
    group_id: json["group_id"],
  );
}
