class AppStrings {
  static const String login = "Login";
  static const String register = "Register";
  static const String loginMessage = "Login now to see your friends posts";
  static const String email = "Email Address";
  static const String password = "Password";
  static const String emailRequired = "Email is required";
  static const String name = "Name";
  static const String nameRequired = "Name is required";
  static const String passwordTooShort = "Password is too short";
  static const String loginError =
      "Login failed, please check your credentials and try again";
  static const String alreadyHaveAccount = "Already have an account?";
  static const String loginNow = "Login now";
  static const String dontHaveAccount = "Don't have an account?";
  static const String registerNow = "Register now";
  static const String bio = "Bio";
  static const String home = "Home";
  static const String groups = "Groups";
  static const String profile = "Profile";
  static const String whatYouHaveInMind = "What do you have in mind...?";
  static const String groupName = "Group name";
  static const String leaveGroupMessage = "Are you sure you want to leave group?";
  static const String ok = "OK";
  static const String cancel = "Cancel";
}
