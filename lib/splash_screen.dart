import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/UI/auth/login_screen.dart';
import 'package:groupaty_flutter/UI/main_screen.dart';
import 'package:groupaty_flutter/cubit/app/cubit.dart';
import 'package:groupaty_flutter/cubit/app/states.dart';
import 'package:groupaty_flutter/shared_functions.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return BlocConsumer<AppCubit, AppStates>(
        listener: (context, state) {
          if (state is CachedUserStateIs) {
            if (state.userID != null) {
              navigateAndClear(MainScreen(), context);
            } else {
              navigateAndClear(LoginScreen(), context);
            }
          }
        },
        builder: (context, state) {
          return Scaffold(
            body: Container(
              color: Colors.teal,
              child: Center(
                child: Text(
                  "GROUPATY",
                  style: TextStyle(
                    fontSize: 32,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          );
        },
      );
    });
  }
}
