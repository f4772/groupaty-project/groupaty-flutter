import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:groupaty_flutter/cubit/app/cubit.dart';
import 'package:groupaty_flutter/cubit/app/states.dart';
import 'package:groupaty_flutter/cubit/auth/cubit.dart';
import 'package:groupaty_flutter/cubit/bloc_observer.dart';
import 'package:groupaty_flutter/cubit/groups/cubit.dart';
import 'package:groupaty_flutter/cubit/home/cubit.dart';
import 'package:groupaty_flutter/cubit/profile/cubit.dart';
import 'package:groupaty_flutter/network/api_manager.dart';
import 'package:groupaty_flutter/network/cache_manager.dart';
import 'package:groupaty_flutter/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DioHelper.init();
  await CacheManager.init();
  Bloc.observer = MyBlocObserver();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => AppCubit()..checkForCacheduser(),
        ),
        BlocProvider(
          create: (BuildContext context) => AuthCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => GroupsCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => ProfileCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => HomeCubit(),
        ),
      ],
      child: BlocConsumer<AppCubit, AppStates>(
        listener: (context, state) {},
        builder: (BuildContext context, state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: SplashScreen(),
          );
        },
      ),
    );
  }
}
